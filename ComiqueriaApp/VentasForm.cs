﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComiqueriaLogic;

namespace ComiqueriaApp
{
    public partial class VentasForm : Form
    {
        private Producto productoSeleccionado;
        private Comiqueria comiqueria;

        public VentasForm()
        {
            InitializeComponent();


        }

        public VentasForm(Producto productoSeleccionado, Comiqueria comiqueria) : this()
        {
            this.comiqueria = comiqueria;
            this.productoSeleccionado = productoSeleccionado;
        }

        private void Label2_Click(object sender, EventArgs e)
        {
        }

        private void LblDescription_Click(object sender, EventArgs e)
        {
        }

        private void LblPrecioFinal_Click(object sender, EventArgs e)
        {
        }

        private void NumericUpDownCantidad_ValueChanged(object sender, EventArgs e)
        {
            this.lblPrecioFinal.Text = "Precio final: " +Venta.CalcularPrecioFinal(productoSeleccionado.Precio, (int) numericUpDownCantidad.Value).ToString("0.##");
        }

        private void VentasForm_Load(object sender, EventArgs e)
        {
            this.lblDescripcion.Text = productoSeleccionado.Descripcion;
            this.lblPrecioFinal.Text = "Precio final: " +Venta.CalcularPrecioFinal(productoSeleccionado.Precio, 1).ToString("0.##");
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
           this.Close();
        }

        private void BtnVender_Click(object sender, EventArgs e)
        {
            if (numericUpDownCantidad.Value <= productoSeleccionado.Stock)
            {
                this.comiqueria.Vender(productoSeleccionado, (int) numericUpDownCantidad.Value);
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBoxIcon icon = MessageBoxIcon.Error;
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Superó el stock disponible.", "Stock", buttons, icon);
            }
        }
    }
}