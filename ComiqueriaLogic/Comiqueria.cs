using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace ComiqueriaLogic
{
    public class Comiqueria
    {
        private List<Producto> productos;
        private List<Venta> ventas;

        public Comiqueria()
        {
            productos = new List<Producto>();
            ventas = new List<Venta>();
        }

        public static bool operator ==(Comiqueria comiqueria, Producto producto)
        {
            foreach (Producto p in comiqueria.productos)
            {
                if (0 == string.Compare(p.Descripcion, producto.Descripcion))
                {
                    return true;
                }

            }

            return false;

        }

        public static bool operator !=(Comiqueria comiqueria, Producto producto)
        {
            return !(comiqueria == producto);
        }

        public static Comiqueria operator +(Comiqueria comiqueria, Producto producto)
        {
            if (comiqueria != producto)
            {
                comiqueria.productos.Add(producto);
                return comiqueria;
            }
            return comiqueria;
        }

        public void Vender(Producto producto, int cantidad)
        {
            if (!Object.ReferenceEquals(producto, null))
            {
                ventas.Add(new Venta(producto, cantidad));

            }
        }
        
        public void Vender(Producto p)
        {
            Vender(p, 1);
            
        }

        public string ListarVentas()
        {
            List<Venta> orderVentas = new List<Venta>(ventas.OrderByDescending(venta => venta.Fecha));
            string toString = "";
            foreach (Venta venta in orderVentas)
            {
                toString += venta.ObtenerDescripcionBreve();
            }
            return toString;
        }

        public Dictionary<Guid, string> ListarProductos()
        {
            Dictionary<Guid, string> dictionary = new Dictionary<Guid, string>();
            foreach (Producto producto in productos)
            {
                dictionary.Add((Guid) producto , producto.Descripcion);
            }

            return dictionary;
        }

        public Producto this[Guid codigoProducto]
        {
            get
            {
                foreach (Producto p in productos)
                {
                    if ((Guid) p == codigoProducto)
                    {
                        return p;
                    }
                }

                return null;
            }
        }
    }
}