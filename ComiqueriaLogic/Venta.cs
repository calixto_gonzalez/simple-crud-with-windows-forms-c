using System;
using System.Text;

namespace ComiqueriaLogic
{
    public sealed class Venta
    {
        private DateTime fecha;
        private static int porcentajeIva;
        private double precioFinal;
        private Producto producto;

        internal DateTime Fecha { get; }

        static Venta()
        {
            porcentajeIva = 21;
        }

        internal Venta(Producto producto, int cantidad)
        {
            this.producto = producto;
            Vender(cantidad);
        }

        private void Vender(int cantidad)
        {
            producto.Stock -= cantidad;
            fecha = DateTime.Now;
            precioFinal = CalcularPrecioFinal(producto.Precio, cantidad);
        }

        public static double CalcularPrecioFinal(double productoPrecio, int cantidad)
        {
            return productoPrecio * cantidad + productoPrecio * cantidad * porcentajeIva / 100;
        }

        public string ObtenerDescripcionBreve()
        {
            return new StringBuilder().AppendFormat("{0} - {1} - {2:00}\n", fecha, producto.Descripcion, precioFinal)
                .ToString();
        }
        
    }
}