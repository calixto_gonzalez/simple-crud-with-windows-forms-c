﻿

using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace ComiqueriaLogic
{
    public abstract class Producto
    {
        private Guid codigo;
        private string descripcion;
        private double precio;
        private int stock;
        
        public string Descripcion => descripcion;

        public double Precio => precio;

        public int Stock
        {
            get { return stock; }
            set
            {
                if (value >= 0)
                {
                    stock = value;
                }
            }
        }

        protected Producto(string descripcion, int stock, double precio)
        {
            codigo = Guid.NewGuid();
            this.descripcion = descripcion;
            this.precio = precio;
            this.stock = stock;
        }

        public static explicit operator Guid(Producto p)
        {
            return p.codigo;
        }

        public override string ToString()
        {
            return new StringBuilder().AppendFormat("Descripción: {0}\nCódigo: {1}\nPrecio: {2}\nStock {3}\n", descripcion, codigo, precio, stock).ToString();
        }
    }
}