using System;
using System.Text;

namespace ComiqueriaLogic
{
    public class Figura : Producto
    {
        private double altura;

        public Figura(string descripcion, int stock, double precio, double altura) : base(descripcion, stock, precio)
        {
            this.altura = altura;
        }

        public Figura(int stock, double precio, double altura) : this(String.Format("Figura {0} cm", altura), stock,
            precio, altura)
        {
            
        }

        public override string ToString()
        {
            return new StringBuilder(base.ToString()).AppendFormat("Altura: {0}\n",altura).ToString();
        }
    }
}