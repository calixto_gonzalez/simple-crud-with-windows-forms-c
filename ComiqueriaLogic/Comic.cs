using System.Text;

namespace ComiqueriaLogic
{
    public class Comic : Producto
    {
        private string autor;
        private TipoComic tipoComic;
        
        public Comic(string descripcion, int stock, double precio, string autor, TipoComic tipoComic) : base(descripcion, stock, precio)
        {
            this.autor = autor;
            this.tipoComic = tipoComic;
        }

        public override string ToString()
        {
            return new StringBuilder(base.ToString()).AppendFormat("Autor: {0}\nTipoComic: {1}", autor, tipoComic).ToString();
        }

        public enum TipoComic
        {
            Occidental,Oriental
        }
    }
}